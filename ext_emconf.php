<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "fancyboxcontent".
 *
 * Auto generated 31-12-2018 10:13
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'fancyBox content',
  'description' => 'Inserts a fancybox plugin for content',
  'category' => 'plugin',
  'author' => 'Raphael Zschorsch',
  'author_email' => 'rafu1987@gmail.com',
  'state' => 'beta',
  'uploadfolder' => false,
  'createDirs' => '',
  'clearCacheOnLoad' => 0,
  'version' => '9.4.0',
  'constraints' => 
  array (
    'depends' => 
    array (
      'extbase' => '9.3.0-9.99.99',
      'fluid' => '9.3.0-9.99.99',
      'typo3' => '9.3.0-9.99.99',
      'php' => '7.2.0-7.2.99',
      'vhs' => '',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
  'autoload' => 
  array (
    'psr-4' => 
    array (
      'RZ\\Fancyboxcontent\\' => 'Classes',
    ),
  ),
  'clearcacheonload' => false,
  'author_company' => NULL,
);

